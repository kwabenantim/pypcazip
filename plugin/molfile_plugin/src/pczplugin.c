/**************************************
Basic pcz format reader plugin for VMD

This supports the PCZ1 format version only.
Author: Charlie Laughton
Version: 0.1
Date: 16/11/2016
***************************************/

#include "largefiles.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "molfile_plugin.h"
#include "endianswap.h"

typedef struct {
  FILE *fd;
  int wrongendian;
  int numatoms;
  int numbonds;
  int numvecs;
  int numframes;
  int nextframe;
  const char *filename;
  int* serials;
  char* anames;
  char* rnames;
  int* resseq;
  char* chains;
  short* bonds;
  float* acme;
  float* ref_coord;
  float* xyz;
  float* evecs;
  float* evals;
  float* p0;
  float* pinc;
  float rproj;
} pczdata;

static void *open_pcz_read(const char *filename, const char *filetype,
                           int *natoms) {
  pczdata *data;
  FILE *fd;
  int er=0, point, igarb;
  float rgarb;
  float *rptr;
  char lenbuf[81];
  char magicchar[5];
  int i, numatoms, numbonds, numvecs, numframes, status;

  fd = fopen(filename, "rb");
  if (!fd)
   {
    fprintf(stderr, "Could not open file '%s' for reading.\n", filename);
    return NULL;
   }
  data = (pczdata *)(malloc(sizeof(pczdata)));
  memset(data, 0, sizeof(pczdata));
  fread(magicchar, sizeof(char), 4, fd);
  magicchar[4] = '\0';
  if(strcmp(magicchar, "PCZ1")!=0)
   {
    fprintf(stderr, "not a pcz format file\n");
    return NULL;
   }
  fread(lenbuf, sizeof(char), 80, fd);
  fread(&igarb, sizeof(int), 1, fd);
  point = ftell(fd);

  /* Endianism check */
  if(igarb>1000000000)
   {
    fprintf(stderr, "File '%s' appears to be other-endian.\n", filename);
    data->wrongendian = 1;
    swap4_aligned(&igarb, 1);

    if((fseek(fd, point, SEEK_SET))!=0)
     {
      fprintf(stderr, "Endian correction failed. er=%d\n", er);
      return NULL;
     }
    fseek(fd, point, SEEK_SET);
  }
  data->numatoms = igarb;
  numatoms = data->numatoms;
  fread(&igarb, sizeof(int), 1, fd);
  if (data->wrongendian) swap4_aligned(&igarb, 1);
  data->numbonds = igarb;
  numbonds = data->numbonds;
  fread(&igarb, sizeof(int), 1, fd);
  if (data->wrongendian) swap4_aligned(&igarb, 1);
  data->numframes = igarb;
  numframes = data->numframes;
  fread(&igarb, sizeof(int), 1, fd);
  if (data->wrongendian) swap4_aligned(&igarb, 1);
  data->numvecs = igarb;
  numvecs = data->numvecs;
  data->nextframe = 0;
  fread(&rgarb, sizeof(float), 1, fd);

  data->serials = calloc(numatoms, sizeof(int));
  if (fread(data->serials, sizeof(int), numatoms, fd)!=(size_t)(numatoms))
   {
    fprintf(stderr, "Failed to read serials data.\n");
    return NULL;
   }

  data->anames = calloc(numatoms, sizeof(char) * 4);
  if (fread(data->anames, sizeof(char) * 4, numatoms, fd)!=(size_t)(numatoms))
   {
    fprintf(stderr, "Failed to read aname data.\n");
    return NULL;
   }

  data->rnames = calloc(numatoms, sizeof(char) * 3);
  status =  fread(data->rnames, sizeof(char) * 3, numatoms, fd);
  if (status!=(size_t)(numatoms))
   {
    fprintf(stderr, "Failed to read rname data.\n");
    return NULL;
   }
  data->resseq = calloc(numatoms, sizeof(int) );
  if (fread(data->resseq, sizeof(int), numatoms, fd)!=(size_t)(numatoms))
   {
    fprintf(stderr, "Failed to read resseq data.\n");
    return NULL;
   }

  data->chains = calloc(numatoms, sizeof(char) );
  if (fread(data->chains, sizeof(char), numatoms, fd)!=(size_t)(numatoms))
   {
    fprintf(stderr, "Failed to read chain data.\n");
    return NULL;
   }

  data->bonds = calloc(numbonds * 2, sizeof(short));
  if (fread(data->bonds, sizeof(short), 2 * numbonds, fd)
            !=(size_t)(2 * numbonds))
    {
    fprintf(stderr, "Failed to read bond data.\n");
    return NULL;
    }

  data->acme = calloc(numatoms * 3, sizeof(float));
  if (fread(data->acme, sizeof(float), 3 * numatoms, fd)
             !=(size_t)(3 * numatoms))
   {
    fprintf(stderr, "Failed to read acme data.\n");
    return NULL;
   }

  data->ref_coord = calloc(numatoms * 3, sizeof(float));
  data->xyz = calloc(numatoms * 3, sizeof(float));
  if (fread(data->ref_coord, sizeof(float), 3 * numatoms, fd)
             !=(size_t)(3 * numatoms))
   {
    fprintf(stderr, "Failed to read refcoord data.\n");
    return NULL;
   }

  data->evals = calloc(numvecs, sizeof(float));
  if (fread(data->evals, sizeof(float), numvecs, fd)
             !=(size_t)(numvecs))
   {
    fprintf(stderr, "Failed to read eval data.\n");
    return NULL;
   }

  data->evecs = calloc(numatoms * numvecs * 3, sizeof(float));

  rptr = data->evecs;
  for (i=0; i<numvecs; i++, rptr += (3 * numatoms)) {
      status = fread(rptr, sizeof(float),  (numatoms * 3), fd);
      if (status !=(size_t)(numatoms *3))
       {
        fprintf(stderr, "Failed to read eigenvector data.\n");
        return NULL;
       }
  }
  data->p0 = calloc(numvecs, sizeof(float));
  if (fread(data->p0, sizeof(float), numvecs, fd)
             !=(size_t)(numvecs))
   {
    fprintf(stderr, "Failed to read p0 data.\n");
    return NULL;
   }

  data->pinc = calloc(numvecs, sizeof(float));
  if (fread(data->pinc, sizeof(float), numvecs, fd)
             !=(size_t)(numvecs))
   {
    fprintf(stderr, "Failed to read pinc data.\n");
    return NULL;
   }

  if (data->wrongendian)
   {
    swap4_aligned(data->ref_coord, 3*numatoms);
    swap4_aligned(data->acme, 3*numatoms);
    swap4_aligned(data->serials, numatoms);
    swap4_aligned(data->resseq, numatoms);
    swap2_aligned(data->bonds, 2*numbonds);
    swap4_aligned(data->evals, numvecs);
    swap4_aligned(data->evecs, numvecs*numatoms*3);
    swap4_aligned(data->p0, numvecs);
    swap4_aligned(data->pinc, numvecs);
   }

  data->fd = fd;
  *natoms = data->numatoms;
  return data;
}


static int read_pcz_structure(void *mydata, int *optflags,
                              molfile_atom_t *atoms) {
  int i;
  molfile_atom_t *atom;
  pczdata *data = (pczdata *)mydata;

  printf("pcz) trying to read structure.\n");
  *optflags = MOLFILE_NOOPTIONS;
  
  for(i=0; i<data->numatoms; i++) {
    atom = atoms + i;
    strncpy(atom->name, &data->anames[i*4], 4);
    strncpy(atom->resname, &data->rnames[i*3], 3);
    strcpy(atom->type, atom->name);
    atom->resid = data->resseq[i];
    strncpy(atom->chain, &data->chains[i], 1);
    atom->segid[0] = '\0';
  }
  return MOLFILE_SUCCESS;
}

static int read_pcz_timestep(void *mydata, int natoms, molfile_timestep_t *ts)
{
  int i, ivec, p_offset, ev_offset, iframe;
  float rproj;
  short *proj;

  pczdata *data = (pczdata *)mydata;
  if (natoms != data->numatoms) {
      fprintf(stderr, "pcz) mismatch in expected number of atoms.\n");
      return MOLFILE_ERROR;
  }
  proj = calloc(data->numvecs, sizeof(short));
  if (fread(proj, sizeof(short), data->numvecs, data->fd)
             !=(size_t)(data->numvecs))
   {
    fprintf(stderr, "Failed to read proj data.\n");
    return MOLFILE_EOF;
   }

  if (data->wrongendian) swap2_aligned(proj, data->numvecs);
  iframe = data->nextframe;
  p_offset = iframe * data->numvecs;
    
  for (i=0; i < natoms * 3; i++) 
    data->xyz[i] = data->ref_coord[i];

  for (ivec=0; ivec < data->numvecs; ivec++) {
    ev_offset = ivec * data->numatoms * 3;
    rproj = (float)proj[ivec] * data->pinc[ivec] + data->p0[ivec];
    for (i=0; i < 3 * data->numatoms; i++) {
         data->xyz[i] += data->evecs[i + ev_offset] * rproj;
    }
  }
  for (i=0; i < natoms * 3; i++) 
    ts->coords[i] = data->xyz[i];

  data->nextframe += 1;
  /*
  if (data->nextframe == data->numframes) {
    close(data->fd);
    return MOLFILE_EOF;
    }
  else
  */
    return MOLFILE_SUCCESS;
}

static void close_pcz_read(void *mydata) {
  pczdata *data = (pczdata *)mydata;
  
  fclose(data->fd);
  free(data);
}


static molfile_plugin_t plugin;

VMDPLUGIN_API int VMDPLUGIN_init() {
  memset(&plugin, 0, sizeof(molfile_plugin_t));
  plugin.abiversion = vmdplugin_ABIVERSION;
  plugin.type = MOLFILE_PLUGIN_TYPE;
  plugin.name = "pcz";
  plugin.prettyname = "PCZ";
  plugin.author = "Charlie Laughton";
  plugin.majorv = 0;
  plugin.minorv = 1;
  plugin.is_reentrant = VMDPLUGIN_THREADSAFE;
  plugin.filename_extension = "pcz";
  plugin.open_file_read = open_pcz_read;
  plugin.read_structure = read_pcz_structure;
  plugin.read_next_timestep = read_pcz_timestep;
  plugin.close_file_read = close_pcz_read;
  return VMDPLUGIN_SUCCESS;
}


VMDPLUGIN_API int VMDPLUGIN_register(void *v, vmdplugin_register_cb cb) {
  (*cb)(v, (vmdplugin_t *)&plugin);
  return VMDPLUGIN_SUCCESS;
}


VMDPLUGIN_API int VMDPLUGIN_fini() {
  return VMDPLUGIN_SUCCESS;
}


