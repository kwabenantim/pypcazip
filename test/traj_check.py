#!/usr/bin/env python
#
# Compares two trajectory files, to see if they "agree".
#
# "Agreement" means:
#    a) They contain the same number of snapshots
#    b) All pairs of snapshots have an rmsd of less than tol

from __future__ import absolute_import, print_function, division

import warnings

with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    import mdtraj as mdt
    from MDPlus.fastfitting import rmsd
    import sys

    topfile = sys.argv[1]
    trajfile1 = sys.argv[2]
    trajfile2 = sys.argv[3]
    tol = float(sys.argv[4])

    u1 = mdt.load(trajfile1, top=topfile)
    u2 = mdt.load(trajfile2, top=topfile)

    n1 = len(u1.xyz)
    n2 = len(u2.xyz)
    if  n1 != n2:
        print("Error: only {0} snapshots in first file but {1} in second.".format(n1,n2))
        exit(1)

    for i in range(n1):
       dif = rmsd(u1.xyz[i], u2.xyz[i])
       if dif > tol:
           print("Error: at snapshot {0} the two coordinate sets have an RMSD greater than {1} (value={2}).".format(i, tol, dif))
           exit(1)
