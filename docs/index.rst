.. MDPlus documentation master file, created by
   sphinx-quickstart on Thu Jun 16 23:02:42 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MDPlus's documentation!
==================================

*MDPlus* is the Python library that powers the **pyPcazip** package command line
tools, and can be used independently.

*MDPlus.core* provides the *Fasu* and *Cofasu* classes that implement fast and
flexible I/O on trajectory files. It is built on top of *MDTraj*. A particular
feature of the Fasu/Cofasu system is that it allows large collections of
potentially disparate trajectory data to be packaged together into a single
data object for integrated analysis.

Example::

    f1 = Fasu('trajectory1.dcd', top='protein.pdb', selection='name CA') # select backbone atoms only
    f2 = Fasu(trajectory+water.xtc', top='protein+water.pdb', selection='name CA')
    trajdata = Cofasu([f1, f2]) # create a single trajectory array from both files.

    # trajdata behaves like a numpy array:
    print trajdata.shape
    frame2 = trajdata[2]
    tenframes = trajdata[:10]
    mean_structure = trajdata[:].mean(axis=0)

    # But it has a couple of extra methods:
    trajdata.align() # least-squares fits all the frames to the first
    trajdata.write('bothtrajfiles.nc') # writes out all frames in Amber netcdf format.


*MDPlus.analysis.pca* provides the *Pcamodel* class that describes the
Principal Component Analysis of some trajectory data, and methods to create 
and read the compressed data files produced by the process.

Example::

    pcaresult = pca.fromtrajectory(trajdata) # trajdata is an mdtraj
                                               trajectory (or similar).
    print pcaresult.totvar # The total variance in the trajectory data
    print pcaresult.n_vecs # The number of eigenvectors required to capture 
                             90% of the total variance.
    pcaresult.write('pcaresults.pcz') # Write the results to a file
    pcaresults2 = pca.load('pcaresults.pcz') # Read them back in again
    print pcaresults2.evecs[0] # print the first eigenvector
    scores7 = pcaresults2.scores(7) # The PC scores for frame 7
    crds7 = pcaresults2.frame(7) # Reconstruct the coordinates of frame 7


*MDPlus.analysis.mapping* provides utilities to explore PCA data as
distributions in a multidimensional space.

Example::

    mymap = mapping.Map(pcaresults,projs[:2]) # map the trajectory data using the projections of the firt three PCs
    print mymap.volume # print the volume of conformational space that has been sampled.
    mapping.watershed(mymap) # Do watershed clustering
    print mymap.cluster_size(1) # Print the number of frames in the largest cluster.


The examples above just scratch the surface of what *MDPlusi* can enable, see 
the API documentation for full details.

Contents:

.. toctree::
   :maxdepth: 4

   api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

